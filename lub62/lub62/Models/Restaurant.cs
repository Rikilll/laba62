﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace lub62.Models
{
    public class Restaurant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string Foto { get; set; }
        public IEnumerable<Dish> Dishes { get; set; }
        public IEnumerable<Bascket> Basckets { get; set; }
    }
}
