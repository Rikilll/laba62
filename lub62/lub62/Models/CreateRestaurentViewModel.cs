﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lub62.Models
{
    public class CreateRestaurentViewModel
    {
        public string Name { get; set; }
        public IFormFile Foto { get; set; }
        public string Info { get; set; }

    }
}
