﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace lub62.Models
{
        public class Dish
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Price { get; set; }
            public string Info { get; set; }
            [ForeignKey("Restaurant")]
            public int RestaurantId { get; set; }
            public Restaurant Restaurant { get; set; }
        }

}
