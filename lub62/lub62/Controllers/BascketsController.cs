﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lub62.Data;
using lub62.Models;
using Microsoft.AspNetCore.Identity;

namespace lub62.Controllers
{
    public class BascketsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public BascketsController(ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }


        [HttpPost]
        public async Task<IActionResult> CreateBascket(int dishId, int restaurantId, int bascketId)
        {
            Bascket bascket = await _context.Bascket.FirstOrDefaultAsync(c => c.Id == bascketId);
            
            Restaurant restaurant = await _context.Restaurant.FirstOrDefaultAsync(c => c.Id == restaurantId);
            Dish dish = await _context.Dish.FirstOrDefaultAsync(c => c.Id == dishId);
            // List<Dish> dishes = await _context.Dish.Where(c => c.RestaurantId == restaurantId).ToListAsync();

            bascket.Dishes.Add(dish);
            
            return View(bascket);
        }
    }
}
