﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lub62.Data;
using lub62.Models;
using System.IO;
using lub62.Service;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;


namespace lub62.Controllers
{
    public class RestaurantsController : Controller
    {
        
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;
        private readonly ApplicationDbContext _context;

        public RestaurantsController( FileUploadService fileUploadService, IHostingEnvironment environment, UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {
            
            _environment = environment;
            _userManager = userManager;
            _fileUploadService = fileUploadService;
            _context = context;
        }
    

        public async Task<IActionResult> IndexOneRestourant(int  id)
        {

            var user = await _userManager.GetUserAsync(User);
            List<Dish> dishesInBascket = new List<Dish>();
            Bascket bascket = new Bascket()
            {
                RestaurantId = id,
                ApplicationUserId = user.Id,
                Dishes = dishesInBascket
            };

            _context.Add(bascket);
            await _context.SaveChangesAsync();

            Restaurant restaurant =await _context.Restaurant.FirstOrDefaultAsync(c => c.Id == id);
            List<Dish> dishes = _context.Dish.Where(d => d.RestaurantId == restaurant.Id).ToList();
            ViewBag.dishesInRestaurant = dishes;
            BascketOfRestaurantViewModel model = new BascketOfRestaurantViewModel()
            {
                Restaurant = restaurant,
                Bascket = bascket
            };
            if (restaurant == null)
            {
                return NotFound();
            }
            return View(model);
        }
        // GET: Restaurants
        public async Task<IActionResult> Index()
        {
            return View(await _context.Restaurant.ToListAsync());
        }

      

        // GET: Restaurants/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Restaurants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateRestaurentViewModel model)
        {
            var path = Path.Combine(_environment.WebRootPath, $"image\\{model.Name}");
            _fileUploadService.Upload(path, model.Foto.FileName, model.Foto);
            Restaurant restaurant = new Restaurant()
            {
                Foto = $"image/{model.Name}/{model.Foto.FileName}",
                Name = model.Name,
                Info = model.Info
            };
            if (ModelState.IsValid)
            {
                _context.Add(restaurant);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(restaurant);
        }

        // GET: Restaurants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurant.FindAsync(id);
            if (restaurant == null)
            {
                return NotFound();
            }
            return View(restaurant);
        }

        // POST: Restaurants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Info,Foto")] Restaurant restaurant)
        {
            if (id != restaurant.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(restaurant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestaurantExists(restaurant.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(restaurant);
        }

        // GET: Restaurants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurant
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        // POST: Restaurants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await _context.Restaurant.FindAsync(id);
            _context.Restaurant.Remove(restaurant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantExists(int id)
        {
            return _context.Restaurant.Any(e => e.Id == id);
        }
    }
}
