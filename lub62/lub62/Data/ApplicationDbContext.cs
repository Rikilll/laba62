﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using lub62.Models;

namespace lub62.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<lub62.Models.Restaurant> Restaurant { get; set; }
        public DbSet<lub62.Models.Dish> Dish { get; set; }
        public DbSet<lub62.Models.Bascket> Bascket { get; set; }
    }
}
