﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace lub62.Data.Migrations
{
    public partial class three : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bascket_AspNetUsers_ApplicationUserId",
                table: "Bascket");

            migrationBuilder.DropIndex(
                name: "IX_Bascket_ApplicationUserId",
                table: "Bascket");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Bascket",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Bascket",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Bascket_ApplicationUserId",
                table: "Bascket",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bascket_AspNetUsers_ApplicationUserId",
                table: "Bascket",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
