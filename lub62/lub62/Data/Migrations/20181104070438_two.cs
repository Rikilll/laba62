﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace lub62.Data.Migrations
{
    public partial class two : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BascketId",
                table: "Dish",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dish_BascketId",
                table: "Dish",
                column: "BascketId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dish_Bascket_BascketId",
                table: "Dish",
                column: "BascketId",
                principalTable: "Bascket",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dish_Bascket_BascketId",
                table: "Dish");

            migrationBuilder.DropIndex(
                name: "IX_Dish_BascketId",
                table: "Dish");

            migrationBuilder.DropColumn(
                name: "BascketId",
                table: "Dish");
        }
    }
}
